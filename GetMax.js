// result is returned as asked in task
//Write a method GetMax() with two parameters that returns the larger of two integers.
//Write a program that reads 3 integers from the console and prints the largest of them using the method GetMax().

 function GetMax (a,b,c) {
 	let d = 0;
 	let largest = d;
 	if (d == 0) {
 		if (a>b) {
 			largest = a;
 		} else  {
 			largest = b;
 		}	
 	}
 	if (c > largest) {
 		largest = c;
 	}
 	return largest; 
 }
 console.log(GetMax(5,2));

//Prototyping an array for GetMax method. !!! not sure if this is the best method of doing this.......
Array.prototype.GetMax = function() {
	let largest = this[array.length-1];
	for(i = 0; i < array.length -1; i += 1){
		if (this[i] > largest) {
			largest = this[i];
		}
	}
	return largest;
};
let array = new Array(1,2,15,4,5);
console.log(array.GetMax());

 //function got getiing largest of all elements given in the input field... 
 function GetMax (...array) {
 	let largest = array[array.length-1];
 	for(i = 0; i < array.length -1; i += 1){
 		if (array[i] > largest) {
 			largest = array[i];
 		}
 	}
	return largest;
 }
 console.log(GetMax(1,2,5,9));