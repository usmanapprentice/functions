function IsBigger (parameter) {
	parameter = parameter[0].split(' ');
	let len = +parameter.shift(), array = parameter.map(x=>+x);
	let count = 0;
	for(let i = 0; i < len; i += 1){
		if (array[i] > array[i+1] && array[i] > array[i-1]) {
			count += 1;
		}
	}
	return count;
}
console.log(IsBigger(['6 -26 -25 -28 31 2 27']));