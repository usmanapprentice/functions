function EnglishDigit (num) {
	num = num.toString();
	num = num.split('').map(x=>+x);
	let word;
	//console.log(num);
	//console.log(num[num.length-1])
	switch (num[num.length-1]) {
		case 1: return word = 'One';
		case 2:	return word = 'Two';
		case 3:	return word = 'Three';
		case 4:	return word = 'Four';
		case 5:	return word = 'Five';
		case 6:	return word = 'Six';
		case 7:	return word = 'Seven';
		case 8:	return word = 'Eight';
		case 9:	return word = 'Nine';
		case 0:	return word = 'Zero';
		default: return word = 'Not a valid entry';
	}
}

console.log(EnglishDigit(49));
